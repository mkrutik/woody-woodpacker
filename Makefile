# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: adzikovs <adzikovs@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/05/22 21:35:20 by mkrutik           #+#    #+#              #
#    Updated: 2019/07/21 10:31:12 by adzikovs         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME1 = woody_woodpacker
NAME2 = /tmp/stub_64

CC = gcc
CFLAGS = -Wall -Werror -Wextra -fPIC

ASM = nasm
AFLAGS = -f elf64

LD = ld
LDFLAGS =

INC = include

SDIR = src

SRCS1 =	\
        packer/main.c \
        packer/libft_helpers.c \
        packer/file_helpers.c \
        packer/elf.c \
        packer/elf_64.c \
        packer/elf_64_parse.c \
        packer/change_headers_64.c \
        packer/find_symbol64.c \
        packer/get_section64.c \
        packer/prepare_loader64.c \
		packer/prepare_loader64_2.c \
        packer/xxtea_encrypt.c

SRCS1_R = $(SRCS1:%.c=$(SDIR)/%.c)

SRCS2 =	\
	stub/loader.asm \
	packer/xxtea_decrypt.c

SRCS2_R = $(SRCS2:%=$(SDIR)/%)

HEADERS1 = \
			defines.h \
			return_codes.h \
			woody.h \

HEADERS1_R = $(HEADERS1:%.h=$(INC)/%.h)

ODIR = Objects

OBJ1 = $(SRCS1:.c=.o)

OBJ1_R = $(OBJ1:%.o=$(ODIR)/%.o)

OBJ2_T = $(SRCS2:%.c=%.o)

OBJ2 = $(OBJ2_T:%.asm=%.o)

OBJ2_R = $(OBJ2:%.o=$(ODIR)/%.o)

all : $(NAME1) $(NAME2)

$(NAME1) : $(ODIR)/packer/elf/x64/ $(OBJ1_R)
	$(CC) $(CFLAGS) -o $(NAME1) $(OBJ1_R)

$(NAME2) : $(ODIR)/packer/elf/x64/ $(ODIR)/stub/ $(OBJ2_R)
	$(LD) $(LDFLAGS) -o $(NAME2) $(OBJ2_R)

.PHONY: clean1 fclean1 dfclean1 re1 clean fclean re

clean1 :
	rm -f $(OBJ1_R)
	rm -rf $(ODIR)

fclean1 : clean1
	rm -f $(NAME1)

re1 : fclean1 $(NAME1)

clean2 :
	rm -f $(OBJ2_R)

fclean2 : clean2
	rm -f $(NAME2)

re2 : fclean2 $(NAME2)

clean: clean1 clean2

fclean : fclean1 fclean2

re : fclean $(NAME1) $(NAME2)

norm:
	norminette $(SRCS1_R) $(HEADERS1_R)

add:
	make add -C libft
	git add $(SRCS1_R) $(HEADERS1_R)\
			$(SRCS2_R) \
			.gitignore Makefile CMakeLists.txt author

$(ODIR)/packer/elf/x64/ :
	mkdir -p $@
	mkdir -p $@/elf
	mkdir -p $@/elf/x64

$(ODIR)/stub/ :
	mkdir -p $@
	mkdir -p $@/stub

$(ODIR)/%.o : $(SDIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@ -I $(INC)

$(ODIR)/%.o : $(SDIR)/%.asm
	$(ASM) $(AFLAGS) -o $@ $<
