segment .text
	global _start
	extern xxtea_decrypt

_start:
    push    rax
    push    rbx
    push    rcx
    push    rdx
    push    rdi
    push    rsi
    
    ; write msg
    mov     rax, 1          ; op code write
    mov     rdi, 1          ; stdout
    lea     rsi, [rel msg]  ; &msg
    mov     rdx, msg_len    ; size
    syscall

    ; get write permission for text section
    lea     rdi, [rel _start]       ; get mem addr of loader
    sub     rdi, [rel prot_offs]    ; get mem addr for mprotect call
    mov     rsi, [rel prot_size]    ; len
    mov     rax, 0xA                ; sys_mprotect
    mov     rdx, 7                  ; PROT_READT | PROT_WRITE | PROT_EXEC
    syscall

    ; decrypt text section
    lea     rdi, [rel _start]       ; get mem addr of loader
    sub     rdi, [rel dec_offs]     ; get mem addr of original text section
    mov     rsi, [rel dec_size]     ; len
    lea     rdx, [rel key]          ; &key
    call xxtea_decrypt

    ; set default memory permissions for text section
    lea     rdi, [rel _start]       ; get mem addr of loader
    sub     rdi, [rel prot_offs]    ; get mem addr of original text section
    mov     rsi, [rel prot_size]    ; len
    mov     rax, 0xA                ; sys_mprotect
    mov     rdx, 5                  ; PROT_READT | PROT_EXEC
    syscall

    pop     rsi
    pop     rdi
    pop     rdx
    pop     rcx
    pop     rbx
    pop     rax

jump:
    jmp 0x04
old_entry:
jump_source_addr:

    msg 		db "....WOODY....", 0xA, 0x0
    msg_len     equ $ - msg
    key 		db "1234567890abcdef"

	dec_offs    dq 0x1111111111111111
	dec_size    dq 0x1212121212121212 ; text section size / 4

	prot_offs   dq 0x1313131313131313
	prot_size   dq 0x1414141414141414
