/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   xxtea_encrypt.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adzikovs <adzikovs@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/29 13:13:51 by adzikovs          #+#    #+#             */
/*   Updated: 2019/07/21 11:01:58 by adzikovs         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include <stddef.h>

#define DELTA 0x9e3779b9
#define BASE(z, y) (((z >> 5) ^ (y << 2)) + ((y >> 3) ^ (z << 4)))
#define POWER(sum, z, y, key, p, e) ((sum ^ y) + (key[(p & 3) ^ e] ^ z))
#define MX(z, y, sum, key, p, e) (BASE(z, y) ^ POWER(sum, z, y, key, p, e))

void	xxtea_encrypt(uint32_t *data, size_t len, const uint32_t *key)
{
	uint32_t	sum;
	uint32_t	coeff[4];
	uint32_t	rounds;

	if ((len - 1) < 1)
		return ;
	sum = 0;
	rounds = (6 + 52 / len);
	coeff[0] = data[((uint32_t)len - 1)];
	while (rounds-- > 0)
	{
		sum += DELTA;
		coeff[3] = sum >> 2 & 3;
		coeff[2] = 0;
		while (coeff[2] < ((uint32_t)len - 1))
		{
			coeff[1] = data[coeff[2] + 1];
			coeff[0] = data[coeff[2]] += MX(coeff[0], coeff[1], sum,
										key, coeff[2], coeff[3]);
			coeff[2]++;
		}
		coeff[1] = data[0];
		coeff[0] = data[((uint32_t)len - 1)] += MX(coeff[0], coeff[1], sum,
												key, coeff[2], coeff[3]);
	}
}
