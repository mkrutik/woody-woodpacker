/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/22 20:42:18 by mkrutik           #+#    #+#             */
/*   Updated: 2019/05/22 21:15:51 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "woody.h"

int	main(int argc, char *argv[])
{
	int get_key_flag;

	get_key_flag = 0;
	if (argc == 3 && ft_strcmp("-k", argv[2]) == 0)
		get_key_flag = 1;
	if (argc == 2 || (argc == 3 && ft_strcmp("-k", argv[2]) == 0))
		elf_handling(argv[1], get_key_flag);
	else
		printf("%sUsage: %s <path to binary file> <-k>\n\n\
		optional arguments:\n-k - for enter encrypt key%s\n",
		ORANGE, argv[0], DEF);
	return (EXIT_SUCCESS);
}
