/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_symbol64.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adzikovs <adzikovs@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/21 10:36:01 by adzikovs          #+#    #+#             */
/*   Updated: 2019/07/21 10:36:01 by adzikovs         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "woody.h"

Elf64_Sym	*find_symbol_by_name64(void *file, const char *name)
{
	Elf64_Shdr	*symtab_hdr;
	Elf64_Sym	*symtab;
	char		*strtab;
	size_t		i;

	symtab_hdr = ((Elf64_Shdr*)get_section_by_name64(file, ".symtab", HDR));
	symtab = (Elf64_Sym*)(file + symtab_hdr->sh_offset);
	strtab = get_section_by_name64(file, ".strtab", SECT);
	i = 0;
	while (((i + 1) * sizeof(*symtab)) <= symtab_hdr->sh_size)
	{
		if (ft_strcmp(strtab + symtab[i].st_name, name) == 0)
			return (symtab + i);
		i++;
	}
	return (NULL);
}

Elf64_Xword	get_symbol_size_by_name64(void *file, const char *name)
{
	Elf64_Shdr	*symtab_hdr;
	Elf64_Sym	*symtab;
	char		*strtab;
	size_t		i;

	symtab_hdr = ((Elf64_Shdr*)get_section_by_name64(file, ".symtab", HDR));
	symtab = (Elf64_Sym*)(file + symtab_hdr->sh_offset);
	strtab = get_section_by_name64(file, ".strtab", SECT);
	i = 0;
	while (((i + 1) * sizeof(*symtab)) <= symtab_hdr->sh_size)
	{
		if (ft_strcmp(strtab + symtab[i].st_name, name) == 0)
			return (symtab[i].st_size);
		i++;
	}
	return (0);
}
