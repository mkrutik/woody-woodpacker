/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   xxtea_decrypt.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adzikovs <adzikovs@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/29 13:14:34 by adzikovs          #+#    #+#             */
/*   Updated: 2019/07/21 12:11:07 by adzikovs         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include <stddef.h>

#define DELTA 0x9e3779b9
#define BASE(z, y) (((z >> 5) ^ (y << 2)) + ((y >> 3) ^ (z << 4)))
#define POW(z, y, sum, key, p, e) ((sum ^ y) + (key[(p & 3) ^ e] ^ z))
#define MX(z, y, sum, key, p, e) (BASE(z, y)^POW(z, y, sum, key, p, e))

void	xxtea_decrypt(uint32_t *data, size_t len, const uint32_t *key)
{
	uint32_t	y;
	uint32_t	sum;
	uint32_t	z;
	uint32_t	p;
	uint32_t	e;

	if ((len - 1) < 1)
		return ;
	y = data[0];
	sum = (6 + 52 / len) * DELTA;
	while (sum > 0)
	{
		e = sum >> 2 & 3;
		p = (uint32_t)len - 1;
		while (p > 0)
		{
			z = data[p - 1];
			y = data[p] -= MX(z, y, sum, key, p, e);
			p--;
		}
		z = data[((uint32_t)len - 1)];
		y = data[0] -= MX(z, y, sum, key, p, e);
		sum -= DELTA;
	}
}
