/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prepare_loader64.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adzikovs <adzikovs@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/21 10:31:50 by adzikovs          #+#    #+#             */
/*   Updated: 2019/07/21 10:35:28 by adzikovs         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "woody.h"

static int	write_16b_key(void *loader, const char arg[KEY_SIZE],
							const char *op_label, const char mask[KEY_SIZE])
{
	Elf64_Sym	*op_sym;
	Elf64_Shdr	*text;
	size_t		offset;
	char		*arg_ptr;
	int			index;

	if (!(op_sym = find_symbol_by_name64(loader, op_label)) ||
		!(text = get_section_by_name64(loader, ".text", HDR)) ||
		op_sym->st_value < text->sh_addr)
		return (WTF);
	offset = text->sh_offset + (op_sym->st_value - text->sh_addr);
	arg_ptr = (char*)(loader) + offset;
	index = 0;
	while (index < KEY_SIZE)
	{
		if (arg_ptr[index] != mask[index])
			return (WTF);
		arg_ptr[index] = arg[index];
		++index;
	}
	return (OK);
}

static int	write_8b_arg64(void *loader, uint64_t arg,
					const char *op_label, uint64_t mask)
{
	Elf64_Sym	*op_sym;
	Elf64_Shdr	*text;
	size_t		offset;
	uint64_t	*arg_ptr;

	if (!(op_sym = find_symbol_by_name64(loader, op_label)) ||
		!(text = get_section_by_name64(loader, ".text", HDR)) ||
		op_sym->st_value < text->sh_addr)
		return (WTF);
	offset = text->sh_offset + (op_sym->st_value - text->sh_addr);
	arg_ptr = (uint64_t*)(loader + offset);
	if (*arg_ptr != mask)
		return (WTF);
	*arg_ptr = arg;
	return (OK);
}

int			write_xxtea_args64(void *loader, Elf64_Addr data,
								Elf64_Xword len, const char key[16])
{
	if (write_8b_arg64(loader, data, "dec_offs", 0x1111111111111111))
		return (WTF);
	if (write_8b_arg64(loader, len, "dec_size", 0x1212121212121212))
		return (WTF);
	if (write_16b_key(loader, key, "key", "1234567890abcdef"))
		return (WTF);
	return (OK);
}

int			write_mprotect_args64(void *loader, Elf64_Addr addr,
									Elf64_Xword len)
{
	if (write_8b_arg64(loader, addr, "prot_offs", 0x1313131313131313))
		return (WTF);
	if (write_8b_arg64(loader, len, "prot_size", 0x1414141414141414))
		return (WTF);
	return (OK);
}
