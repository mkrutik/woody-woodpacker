/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prepare_loader64_2.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adzikovs <adzikovs@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/22 21:30:10 by mkrutik           #+#    #+#             */
/*   Updated: 2019/07/21 10:47:03 by adzikovs         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "woody.h"

static int		write_old_entrypoint64(void *loader, uint32_t old)
{
	Elf64_Sym	*op_sym;
	Elf64_Shdr	*text;
	size_t		offset;
	uint32_t	*arg_ptr;

	if (!(op_sym = find_symbol_by_name64(loader, "old_entry")) ||
		!(text = get_section_by_name64(loader, ".text", HDR)) ||
		op_sym->st_value < text->sh_addr)
		return (WTF);
	offset = text->sh_offset + (op_sym->st_value - text->sh_addr) - 4;
	arg_ptr = (uint32_t*)(loader + offset);
	*arg_ptr = old;
	return (OK);
}

static uint32_t	get_old_entry_rel_addr64(Elf64_Shdr *input_text,
					uint64_t old_entry, void *loader, uint64_t loader_offset)
{
	Elf64_Sym	*op_sym;
	Elf64_Shdr	*loader_text;
	size_t		offset;
	uint32_t	ret;
	int64_t		tmp;

	if (!(op_sym = find_symbol_by_name64(loader, "jump_source_addr")) ||
		!(loader_text = get_section_by_name64(loader, ".text", HDR)) ||
		op_sym->st_value < loader_text->sh_addr)
		return (0);
	offset = loader_offset + (op_sym->st_value - loader_text->sh_addr);
	old_entry = input_text->sh_offset + (old_entry - input_text->sh_addr);
	tmp = (int64_t)(old_entry - offset);
	ret = (uint32_t)((int32_t)tmp);
	return (ret);
}

int				prepare_loader64(void *input, void *loader, const char key[16],
							t_foo param)
{
	uint64_t	old_entrypoint;
	Elf64_Shdr	*text;
	Elf64_Addr	addr;
	Elf64_Xword	len;

	if (!(text = get_section_by_name64(input, ".text", HDR)))
		return (WTF);
	old_entrypoint = ((Elf64_Ehdr*)input)->e_entry;
	old_entrypoint = (uint32_t)get_old_entry_rel_addr64(text, old_entrypoint,
												loader, param.loader_offset);
	if (write_old_entrypoint64(loader, (uint32_t)old_entrypoint))
		return (WTF);
	addr = param.new_entry - ((text->sh_addr / 4096) * 4096);
	len = text->sh_addr % 4096 + text->sh_size;
	if (write_mprotect_args64(loader, addr, len))
		return (WTF);
	if (write_xxtea_args64(loader, (param.loader_offset - text->sh_offset),
		(text->sh_size / 4), key))
		return (WTF);
	return (OK);
}
