/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   change_headers_64.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adzikovs <adzikovs@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/21 11:50:02 by adzikovs          #+#    #+#             */
/*   Updated: 2019/07/21 12:03:58 by adzikovs         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "woody.h"

static int		elf_64_find_phdr(t_elf64_inf *out)
{
	Elf64_Phdr	*phdr;
	uint64_t	i;

	i = 0;
	phdr = out->parsed_bin.phdr;
	while (i < out->parsed_bin.head->e_phnum)
	{
		if (out->parsed_bin.head->e_entry >= phdr[i].p_vaddr &&
			out->parsed_bin.head->e_entry < phdr[i].p_vaddr + phdr[i].p_filesz)
		{
			out->segment_offs = phdr[i].p_offset;
			out->segment_size = phdr[i].p_filesz;
			out->segment_addr = phdr[i].p_vaddr;
			phdr[i].p_filesz += out->stub_text_size;
			phdr[i].p_memsz += out->stub_text_size;
			return (1);
		}
		i++;
	}
	return (0);
}

void			elf_64_change_phdr(t_elf64_inf *out)
{
	uint64_t	i;
	Elf64_Phdr	*phdr;
	int			patched;

	phdr = out->parsed_bin.phdr;
	patched = elf_64_find_phdr(out);
	i = 0;
	while (i < out->parsed_bin.head->e_phnum)
	{
		if (patched == 1 &&
			(phdr[i].p_offset >= (out->segment_offs + out->segment_size)))
			phdr[i].p_offset += PAGE_SIZE;
		i++;
	}
}

static void		elf_64_find_shdr(t_elf64_inf *out)
{
	Elf64_Shdr	*shdr;
	uint64_t	i;

	i = 0;
	shdr = out->parsed_bin.shdr;
	while (i < out->parsed_bin.head->e_shnum)
	{
		if (ft_strcmp(out->parsed_bin.sec_names + shdr[i].sh_name, TEXT) == 0)
		{
			out->sec_size = shdr[i].sh_size;
			out->sec_addr = shdr[i].sh_addr;
			out->sec_offs = shdr[i].sh_offset;
		}
		if ((shdr[i].sh_offset + shdr[i].sh_size) ==
				(out->segment_offs + out->segment_size))
			shdr[i].sh_size += out->stub_text_size;
		i++;
	}
}

void			elf_64_change_shdr(t_elf64_inf *out)
{
	Elf64_Shdr	*shdr;
	uint64_t	i;
	uint64_t	last_offs;

	elf_64_find_shdr(out);
	shdr = out->parsed_bin.shdr;
	last_offs = out->segment_offs + out->segment_size;
	i = 0;
	while (i < out->parsed_bin.head->e_shnum)
	{
		if (shdr[i].sh_offset > last_offs)
			shdr[i].sh_offset += PAGE_SIZE;
		i++;
	}
}
