/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_helpers.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/22 20:42:10 by mkrutik           #+#    #+#             */
/*   Updated: 2019/05/22 20:42:15 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "woody.h"

size_t	ft_strlen(const char *str)
{
	unsigned int lenght;

	lenght = 0;
	while (str[lenght] != '\0')
		lenght++;
	return (lenght);
}

int		ft_strcmp(const char *s1, const char *s2)
{
	int i;

	i = 0;
	while (s1[i] || s2[i])
	{
		if (s1[i] != s2[i])
			return (s1[i] - s2[i]);
		i++;
	}
	return (0);
}

int		ft_strncmp(const char *src1, const char *src2, size_t n)
{
	if (n == 0)
		return (0);
	while (n-- != 0 && (src1 || src2))
	{
		if (*src1 != *src2)
			return (*(unsigned char*)src1 - *(unsigned char*)src2);
		src1++;
		src2++;
	}
	return (0);
}

void	*ft_memcpy(void *dist, const void *src, size_t n)
{
	unsigned char *pdist;
	unsigned char *psrc;

	pdist = (unsigned char*)dist;
	psrc = (unsigned char*)src;
	while (n--)
	{
		*pdist = *psrc;
		pdist++;
		psrc++;
	}
	return (dist);
}

void	*ft_memset(void *d, int c, size_t len)
{
	unsigned char *pd;

	pd = (unsigned char*)d;
	while (len--)
	{
		*pd = (unsigned char)c;
		pd++;
	}
	return (d);
}
