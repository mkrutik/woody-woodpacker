/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   elf_64_parse.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adzikovs <adzikovs@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/22 20:40:36 by mkrutik           #+#    #+#             */
/*   Updated: 2019/07/21 12:24:52 by adzikovs         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "woody.h"

static void	elf64_check(const t_file *file, const t_elf_64 *out)
{
	uint64_t i;

	if (file == NULL || file->data == NULL || out == NULL)
		ERROR("Brocken function argument");
	i = 0;
	while (i < out->head->e_phnum)
	{
		if (file->size < (out->phdr[i].p_offset + out->phdr[i].p_filesz))
			ERROR("elf64 file too small");
		i++;
	}
}

static void	elf_64_parse(const t_file *file, t_elf_64 *out)
{
	if (file == NULL || file->data == NULL || out == NULL)
		ERROR("Brocken function argument");
	if (file->size < EI_NIDENT)
		ERROR("elf64 file too small");
	if (ft_strncmp((char*)file->data, ELFMAG, ft_strlen(ELFMAG)) != 0)
		ERROR("file isn't elf format");
	if (file->size < sizeof(Elf64_Ehdr))
		ERROR("elf64 file too small");
	out->head = (Elf64_Ehdr*)file->data;
	if (out->head->e_entry == 0)
		ERROR("elf64 must be executable");
	if (out->head->e_version == VERSION_PATCH)
		ERROR("elf64 binary already infected");
	if (file->size < (out->head->e_phoff + out->head->e_phentsize))
		ERROR("elf64 file too small");
	if (out->head->e_phoff != 0)
		out->phdr = (Elf64_Phdr*)(file->data + out->head->e_phoff);
	if (file->size < (out->head->e_shoff + out->head->e_shentsize))
		ERROR("elf64 file too small");
	out->shdr = (Elf64_Shdr*)(file->data + out->head->e_shoff);
	if (out->head->e_shstrndx > out->head->e_shnum)
		ERROR("elf64 brocken table index");
	out->sec_names = (char*)(file->data +
							out->shdr[out->head->e_shstrndx].sh_offset);
	elf64_check(file, out);
}

static void	stub_64_find_text_sec(t_elf64_inf *out)
{
	t_elf_64	elf_inf;
	int			index;

	elf_64_parse(&out->stub, &elf_inf);
	index = -1;
	while (++index < (int)elf_inf.head->e_shnum)
		if (!ft_strncmp(&elf_inf.sec_names[elf_inf.shdr[index].sh_name],
						TEXT, ft_strlen(TEXT)))
		{
			out->stub_text_offs = elf_inf.shdr[index].sh_offset;
			out->stub_text_size = elf_inf.shdr[index].sh_size;
			break ;
		}
	if (index == (int)elf_inf.head->e_shnum)
		ERROR("elf64 stub without text section");
}

void		elf_64_prepare(t_elf64_inf *inf)
{
	elf_64_parse(&inf->bin, &inf->parsed_bin);
	get_file_content(STUB_64_PATH, &inf->stub);
	stub_64_find_text_sec(inf);
}
