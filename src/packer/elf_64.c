/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   elf_64.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adzikovs <adzikovs@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/22 20:41:35 by mkrutik           #+#    #+#             */
/*   Updated: 2019/07/21 12:28:40 by adzikovs         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "woody.h"

static void	patch_64_bin(t_elf64_inf *inf, t_file *res)
{
	uint64_t stub_offs;

	if ((res->data = (uint8_t*)malloc((inf->bin.size + PAGE_SIZE))) == NULL)
		ERROR(strerror(errno));
	res->size = inf->bin.size + PAGE_SIZE;
	stub_offs = inf->segment_offs + inf->segment_size;
	ft_memcpy(res->data, inf->bin.data, stub_offs);
	ft_memset((res->data + stub_offs), 0, PAGE_SIZE);
	ft_memcpy((res->data + stub_offs), (inf->stub.data + inf->stub_text_offs),
				inf->stub_text_size);
	ft_memcpy((res->data + stub_offs + PAGE_SIZE), (inf->bin.data + stub_offs),
			(inf->bin.size - stub_offs));
}

void		elf_64_handling(t_file *file, t_file *res, int key_flag)
{
	t_elf64_inf inf;
	t_foo		bar;

	inf.bin = *file;
	elf_64_prepare(&inf);
	elf_64_change_phdr(&inf);
	(key_flag == 1) ? get_encrypt_key(inf.key) : get_rundom_key(inf.key);
	bar.loader_offset = inf.segment_offs + inf.segment_size;
	bar.new_entry = inf.segment_addr + inf.segment_size;
	if (OK != prepare_loader64(inf.bin.data, inf.stub.data, inf.key, bar))
		ERROR("patching loader failed");
	elf_64_change_shdr(&inf);
	inf.parsed_bin.head->e_version = VERSION_PATCH;
	inf.parsed_bin.head->e_shoff += PAGE_SIZE;
	inf.old_entry = inf.parsed_bin.head->e_entry;
	inf.parsed_bin.head->e_entry = inf.segment_addr + inf.segment_size;
	xxtea_encrypt((inf.bin.data + inf.sec_offs), inf.sec_size / 4,
					(uint32_t*)inf.key);
	patch_64_bin(&inf, res);
	printf("Encrypt key value: %s\n", inf.key);
	release_mem(inf.stub.data, inf.stub.mem_size);
	release_mem(file->data, file->mem_size);
}
