/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   elf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adzikovs <adzikovs@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/22 20:41:27 by mkrutik           #+#    #+#             */
/*   Updated: 2019/07/21 12:20:53 by adzikovs         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "woody.h"

static void	create_file(const t_file *file)
{
	int fd;

	if (file == NULL || file->data == NULL || file->size == 0)
		ERROR("Brocken function argument");
	if ((fd = open(FILE_NAME, O_RDWR | O_CREAT,
					S_IRWXU | S_IRWXG | S_IRWXO)) == -1)
		ERROR(strerror(errno));
	write(fd, file->data, file->size);
	if (close(fd) != 0)
		ERROR(strerror(errno));
	free(file->data);
}

void		elf_handling(const char *file_name, int key_flag)
{
	t_file	file;
	t_file	res;

	get_file_content(file_name, &file);
	if (file.size < EI_NIDENT)
		ERROR("elf64 file too small");
	if (ft_strncmp((char*)file.data, ELFMAG, ft_strlen(ELFMAG)) != 0)
		ERROR("file isn't elf format");
	if (((unsigned char*)file.data)[EI_CLASS] == ELFCLASS32)
		ERROR("32 bit elf doesn't support");
	else if (((unsigned char*)file.data)[EI_CLASS] == ELFCLASS64)
		elf_64_handling(&file, &res, key_flag);
	else
		ERROR("elf file has invalid class");
	create_file(&res);
}
