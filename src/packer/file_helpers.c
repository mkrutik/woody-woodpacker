/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_helpers.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adzikovs <adzikovs@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/22 20:41:45 by mkrutik           #+#    #+#             */
/*   Updated: 2019/07/21 12:27:00 by adzikovs         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "woody.h"

void	get_file_content(const char *file_name, t_file *out)
{
	int fd;
	int size;

	if (file_name == NULL || out == NULL)
		ERROR("Brocken function argument");
	if ((fd = open(file_name, O_RDONLY, 0666)) == -1)
		ERROR(strerror(errno));
	if ((size = lseek(fd, 0, SEEK_END)) == -1)
		ERROR(strerror(errno));
	out->size = (size_t)size;
	out->mem_size = out->size + (PAGE_SIZE - out->size % PAGE_SIZE);
	if ((out->data = (uint8_t*)mmap(0, out->mem_size, PROT_READ | PROT_WRITE,
									MAP_PRIVATE, fd, 0)) == MAP_FAILED)
		ERROR(strerror(errno));
	if (close(fd) != 0)
		ERROR(strerror(errno));
}

void	release_mem(void *ptr, size_t size)
{
	if (munmap(ptr, size) == -1)
		ERROR(strerror(errno));
}

void	get_encrypt_key(char key[KEY_SIZE])
{
	int i;

	ft_memset(&key[0], '0', KEY_SIZE);
	write(STDOUT_FILENO, INVITATION, ft_strlen(INVITATION));
	if ((i = read(STDIN_FILENO, &key[0], KEY_SIZE)) == -1)
		ERROR(strerror(errno));
	if (key[i - 1] == '\n')
		key[i - 1] = '0';
	i = 0;
	while (i < KEY_SIZE)
	{
		if (!(key[i] >= 65 && key[i] <= 90) &&
			!(key[i] >= 48 && key[i] <= 57))
			ERROR("encrypt key has wrong format");
		++i;
	}
}

void	get_rundom_key(char key[16])
{
	uint8_t	random[8];
	int		fd;
	int		i;
	int		j;

	if ((fd = open("/dev/urandom", O_RDONLY, 0666)) == -1)
		ERROR(strerror(errno));
	if (read(fd, &random[0], 8) == -1)
		ERROR(strerror(errno));
	if (close(fd) != 0)
		ERROR(strerror(errno));
	i = 0;
	j = 0;
	while (i < 8)
	{
		if (j < 16)
		{
			key[j++] = HEX_LINE[random[i] % 16];
			key[j++] = HEX_LINE[(random[i] / 16) % 16];
		}
		i++;
	}
}
