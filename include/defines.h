/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   defines.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adzikovs <adzikovs@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/25 13:19:13 by adzikovs          #+#    #+#             */
/*   Updated: 2019/03/26 19:20:28 by adzikovs         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOODPACKER_DEFINES_H
# define WOODPACKER_DEFINES_H

# define HDR 0
# define SECT 1

# define STUB_64_PATH "/tmp/stub_64"

# define INVITATION "Please enter encrypt key (16 hex characters in uppcase): "
# define KEY_SIZE 16

# define HEX_LINE "0123456789ABCDEF"
# define TEXT ".text"
# define FILE_NAME "woody"
# define PAGE_SIZE sysconf(_SC_PAGESIZE)
# define VERSION_PATCH 0x12345678

# define PHNUM64(p) (((Elf64_Ehdr*)p)->e_phnum)
# define PHDRS64(p) ((Elf64_Phdr*)((void*)p + ((Elf64_Ehdr*)p)->e_phoff))
# define SECT_HDRS64(p) ((Elf64_Shdr*)((void*)p + ((Elf64_Ehdr*)p)->e_shoff))

#endif
