/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   woody.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adzikovs <adzikovs@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/22 21:19:58 by mkrutik           #+#    #+#             */
/*   Updated: 2019/07/21 12:20:18 by adzikovs         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOODY_H
# define WOODY_H

# include <elf.h>
# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>
# include <sys/mman.h>
# include <stdio.h>
# include <string.h>
# include <errno.h>

# include "defines.h"
# include "return_codes.h"

# define ORANGE  "\033[0;33m"
# define DEF     "\033[0m"

# define ERROR(msg) exit(printf("%s: - %s\n", __func__, msg))

typedef struct	s_file
{
	uint8_t		*data;
	size_t		size;
	size_t		mem_size;
}				t_file;

typedef struct	s_elf_64
{
	Elf64_Ehdr	*head;
	Elf64_Shdr	*shdr;
	Elf64_Phdr	*phdr;
	const char	*sec_names;
}				t_elf_64;

typedef struct	s_elf64_inf
{
	t_file		bin;
	t_elf_64	parsed_bin;

	Elf64_Addr	old_entry;
	uint64_t	segment_offs;
	uint64_t	segment_addr;
	uint64_t	segment_size;

	uint64_t	sec_offs;
	uint64_t	sec_size;
	uint64_t	sec_addr;

	t_file		stub;
	uint64_t	stub_text_offs;
	uint64_t	stub_text_size;

	char		key[KEY_SIZE];
}				t_elf64_inf;

typedef struct	s_foo
{
	uint64_t	loader_offset;
	uint64_t	new_entry;
}				t_foo;

/*
** file helper function
*/
void			get_file_content(const char *file_name, t_file *out);
void			release_mem(void *ptr, size_t size);
void			get_encrypt_key(char key[KEY_SIZE]);
void			get_rundom_key(char key[16]);

/*
** ELF handler functions
*/
void			elf_handling(const char *file_name, int key_flag);
void			elf_64_handling(t_file *bin, t_file *res, int key_flag);
void			elf_64_prepare(t_elf64_inf *inf);

/*
** libft helpers
*/
int				ft_strncmp(const char *src1, const char *src2, size_t n);
int				ft_strcmp(const char *s1, const char *s2);
size_t			ft_strlen(const char *str);
void			*ft_memcpy(void *dist, const void *src, size_t n);
void			*ft_memset(void *d, int c, size_t len);

/*
** Patching and encryption functions
*/
void			*get_section_by_index64(void *file, size_t index, char res);
void			*get_section_by_name64(void *file, const char *name, char res);
Elf64_Sym		*find_symbol_by_name64(void *file, const char *name);
void			elf_64_change_phdr(t_elf64_inf *out);
void			elf_64_change_shdr(t_elf64_inf *out);
Elf64_Xword		get_symbol_size_by_name64(void *file, const char *name);
int				prepare_loader64(void *in, void *stub, const char key[16],
								t_foo param);
void			xxtea_encrypt(uint8_t *data, size_t len, const uint32_t *key);
int				write_xxtea_args64(void *loader, Elf64_Addr data,
									Elf64_Xword len, const char key[16]);
int				write_mprotect_args64(void *loader, Elf64_Addr addr,
									Elf64_Xword len);
#endif
