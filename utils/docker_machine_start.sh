#!/bin/bash
cd ~
unlink -f .docker
rm -Rf .docker
docker-machine create -d virtualbox default
docker-machine stop default
cp -R ~/.docker "goinfre/"$USER""_docker/""
rm -rf ~/.docker
ln -s "goinfre/"$USER""_docker/"" .docker
docker-machine start default
