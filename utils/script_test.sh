#!/bin/bash
LIST=$(ls -l /bin/ | sed  '/lr/d' | awk {'print $9'} | sed '/b*z/d')
i=1

LIST="
cat
chgrp
chmod
chown
cp
dash
date
dd
df
dir
dmesg
echo
egrep
false
fgrep
findmnt
grep
hostname
journalctl
kill
less
lessecho
lesskey
lesspipe
ln
login
loginctl
ls
lsblk
mkdir
mknod
mktemp
more
mount
mountpoint
mv
networkctl
ps
pwd
readlink
rm
rmdir
run-parts
sed
sleep
stty
su
sync
systemctl
systemd-ask-password
systemd-escape
systemd-inhibit
systemd-machine-id-setup
systemd-notify
systemd-tmpfiles
systemd-tty-ask-password-agent
tailf
tar"

mkdir -p test_bins

for filename in $LIST; do
	echo ----- $filename
	./woody_woodpacker /bin/$filename
	mv ./woody ./test_bins/"$filename"_woody
	/bin/$filename > "$filename"_orig.txt
	./$filename_woody > "$filename"_woody.txt
	diff "$filename"_orig.txt "$filename"_woody.txt
	diff "$filename"_orig.txt "$filename"_woody.txt > "$filename"_diff.txt
	echo -----
done

